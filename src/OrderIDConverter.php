<?php
/**
 * 支付宝支付网关在对接支付宝支付服务器时，采用支付实体（\Drupal\commerce_payment\Entity\Payment）的ID作为支付订单ID
 * 但由于支付宝要求商户订单号必须小于64个字符，因此发送前必须进行转换
 * 规则为系统实例ID加支付实体ID，不足6位则在中间进行补零处理，超过6位则保持不变
 * 注意：本类不考虑长度超过64位的情况，因为对于实体ID来说无异于天文数字，最大的经济体也不会到达
 */

namespace Drupal\commerce_alipayment;


class OrderIDConverter {

  //订单号补位字符
  static public $padding = '0';

  /**
   * 将系统订单号转换为支付宝订单号
   *
   * @param        $ID       string|int
   * @param string $systemID 订单号前缀，来自系统标志，模式：^[A-Za-z]{0,9}$
   *
   * @return string
   */
  static function toAlipay($ID, $systemID = '') {
    //由于性能原因，不派发钩子，用户不参与订单号管理
    $len = strlen($systemID . $ID);
    if ($len >= 6) {
      return $systemID . $ID;
    }
    $padding = str_repeat(self::$padding, 6 - $len);
    return $systemID . $padding . $ID;
  }

  /**
   * 将支付宝订单号转化为系统订单号
   *
   * @param        $ID       string|int
   * @param string $systemID 订单号前缀，来自系统标志，模式：^[A-Za-z]{0,9}$
   *
   * @return string
   */
  static function toDrupal($ID, $systemID = '') {
    /*
    if (empty($systemID)) {
      return ltrim($ID, self::$padding);
    }
    $ID = substr($ID, strlen($systemID));
    return ltrim($ID, self::$padding);
    */
    //这是为了避免用户在生产站点上更改系统实例ID后导致部分支付通知失效，而出现掉单现象
    //在高并发站点尤为突出
    $ID = preg_replace('/[a-zA-Z]+/', '', $ID);
    return ltrim($ID, self::$padding);
  }

}
