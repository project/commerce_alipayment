<?php
/**
 * 列出退款明细
 */

namespace Drupal\commerce_alipayment\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\commerce_payment\PluginForm\PaymentGatewayFormBase;
use Drupal\commerce_alipayment\Plugin\Commerce\PaymentGateway\Alipay;
use Drupal\Core\Url;

class ListRefundForm extends PaymentGatewayFormBase {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;
    $paymentID = $payment->id();
    $orderEntity = $payment->getOrder();
    $orderID = $orderEntity->id();
    $moduleData = $orderEntity->getData(COMMERCE_ALIPAYMENT_DATA_KEY, []);
    $refundData = $moduleData['refund'][$paymentID] ?: [];
    $priceConverter = \Drupal::service('commerce_price.minor_units_converter');
    $state = [
      Alipay::REFUND_SUCCESS => $this->t('SUCCESS'),
      Alipay::REFUND_NEW     => $this->t('NEW', [], ['context' => 'refund state']),
      Alipay::REFUND_FAIL    => $this->t('FAIL'),
    ];

    $form['refund'] = [
      '#type'       => 'table',
      '#caption'    => $this->t('Payment refund detail'),
      '#header'     => [
        $this->t('state'),
        $this->t('time'),
        $this->t('amount'),
        $this->t('currency'),
        $this->t('data'),
        $this->t('operation'),
      ],
      '#empty'      => $this->t('No Refund'),
      '#sticky'     => TRUE,
      '#attributes' => ['class' => ['commerce-alipay-refund-list']],
    ];
    foreach ($refundData as $time => $data) {
      $form['refund'][$time]['state'] = [
        '#markup' => $state[$data['state']],
      ];
      $form['refund'][$time]['time'] = [
        '#markup' => date('Y-m-d H:i:s', $time),
      ];
      $form['refund'][$time]['amount'] = [
        '#markup' => (string) $priceConverter->fromMinorUnits($data['refund_amount'], $data['currency']),
      ];
      $form['refund'][$time]['currency'] = [
        '#markup' => $data['currency'],
      ];
      $form['refund'][$time]['data'] = [
        '#type'          => 'textarea',
        '#title'         => $this->t('Alipay return:'),
        '#default_value' => print_r($data['data'], TRUE),
        '#attributes'    => [
          'autocomplete' => 'off',
        ],
      ];
      $form['refund'][$time]['operation'] = [
        '#markup' => $this->t('N/A'),
      ];
      if ($data['state'] != Alipay::REFUND_SUCCESS) {//进行一次退款主动查询
        $form['refund'][$time]['operation'] = [
          '#title' => $this->t('Query'),
          '#type'  => 'link',
          '#url'   => Url::fromRoute('commerce_alipayment.queryRefund', ['orderID' => $orderID, 'paymentID' => $paymentID, 'refundID' => $time], ['attributes' => ['class' => ['button']],]),
        ];
      }
    }
    $form['#success_message'] = NULL;
    $form['#process'][] = [get_class($this), 'processForm'];
    return $form;
  }

  public static function processForm(array $form, FormStateInterface $form_state, array &$complete_form) {
    $complete_form['actions']['submit']['#value'] = t('Return');
    $complete_form['actions']['cancel']['#access'] = FALSE;
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
  }

}
