<?php
/**
 * 构建支付页面
 */

namespace Drupal\commerce_alipayment\Form;

use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm as BasePaymentOffsiteForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\commerce_alipayment\AlipayAPI;
use Drupal\commerce_alipayment\OrderIDConverter;
use Drupal\Component\Utility\Unicode;


class PaymentForm extends BasePaymentOffsiteForm {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    /**
     * 该方法在以下方法中被调用：
     * \Drupal\commerce_payment\Plugin\Commerce\CheckoutPane\PaymentProcess::buildPaneForm
     * 本类$this->entity为新建的支付实体（Drupal\commerce_payment\Entity\Payment）
     * 但尚未保存，由于支付宝支付的特点，它的bundle适合设置为payment_manual
     * 返回链接$form['#return_url']已经被设置为路由：commerce_payment.checkout.return，值为绝对路径字符串，如：
     * 'http://shop.dp.com/checkout/22/payment/return'
     * 取消链接$form['#cancel_url']已经被设置为路由：commerce_payment.checkout.cancel，值为绝对路径字符串，如：
     * 'http://shop.dp.com/checkout/22/payment/cancel'
     * 异步通知路由为：commerce_payment.notify，须在此手动设置，地址如下：
     * /payment/notify/PAYMENT_GATEWAY_ID
     */

    $form = parent::buildConfigurationForm($form, $form_state);
    $payment = $this->entity; //新建的支付实体，尚未保存
    $gatewayEntity = $payment->getPaymentGateway(); //支付网关配置实体
    $gatewayPlugin = $gatewayEntity->getPlugin(); //支付网关插件对象
    $config = $gatewayEntity->getPluginConfiguration();//插件配置，即支付宝API接口配置
    $orderEntity = $payment->getOrder(); //订单实体
    $this->logger = \Drupal::logger('commerce_alipayment');

    $alipayAPI = new AlipayAPI($config, $this->logger);

    /**
     * 我们以系统ID和支付实体的ID做微信商户订单ID，因此必须先保存，如果后续出现各种原因终断的支付，那么这些被保存的支付实体
     * 会成为系统垃圾，不过没关系，长时间弃用的未付款支付实体会被回收机制清理，详见计划任务
     */
    $expiresTime = time() + $config['expires'];
    $payment->expires = $expiresTime;
    $payment->save();

    // 构建订单数组如下：
    $order['order_number'] = OrderIDConverter::toAlipay($payment->id(), $config['systemId']);
    // 必选 商户订单号 长度小于64位 只能是数字、大小写字母_-*且在同一个商户号下唯一
    //以第一个条目对象的标题作为订单描述,截取到固定长度
    $description = '';
    if (isset($orderEntity->getItems()[0])) {
      $description = Unicode::truncate($orderEntity->getItems()[0]->getTitle(), 20, FALSE, TRUE);
    }
    $order['description'] = $description . $this->t('OrderID') . ':' . $orderEntity->id();//用ID是否会导致商业机密泄露，请自行更改
    // 必选 商品描述
    $order['total'] = \Drupal::service('commerce_price.minor_units_converter')->toMinorUnits($payment->getAmount());
    //必选 总金额 int 订单总金额，单位为分。
    $order['currency'] = $payment->getAmount()->getCurrencyCode();
    //可选 货币单位 接口暂时可不用传递该参数 注意支付宝支付境内商户号默认支持人民币：CNY
    $order['notify_url'] = $gatewayPlugin->getNotifyUrl()->toString(FALSE);
    // 必选 支付宝服务器主动通知商户服务器里指定的页面http/https路径。 string[1,256]
    $order['return_url'] = $form['#return_url'];
    //必选 返回链接 付款完成后重定向回来的链接
    $order['timeout_express'] = $expiresTime;
    //必选 超时时间戳 超过该时间交易将无法下单 默认一个月

    $data = ['payment' => $payment->id()];
    //支付完成后传递给返回链接的数据
    $order['return_url'] = Url::fromUri($order['return_url'], ['absolute' => TRUE, 'query' => $data])->toString();
    $response = $alipayAPI->order($order);
    echo $response->getContent();
    die; //支付宝SDK返回的是一个自动提交的表单  这里直接显示  跳过后续系统流程
    //如果不跳过后续流程 可采用响应事件进行处理  不过目前看没有必要
  }

}
