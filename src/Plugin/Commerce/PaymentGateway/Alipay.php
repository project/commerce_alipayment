<?php
/**
 * 支付宝支付网关插件
 *
 * Company: 未来很美（深圳）科技有限公司 site: www.will-nice.com
 * Developer: 云客【云游天下，做客四方】 site: www.indrupal.com
 * 微信号：indrupal
 * Email：phpworld@qq.com
 *
 */

namespace Drupal\commerce_alipayment\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\commerce_price\Price;
use Drupal\Component\Utility\Unicode;
use Drupal\Core\Form\FormStateInterface;
use Drupal\commerce_order\Entity\OrderInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsRefundsInterface;
use Drupal\commerce_alipayment\AlipayAPI;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_alipayment\OrderIDConverter;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * Provides the Alipay payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "alipay",
 *   label = @Translation("Alipay"),
 *   display_label = @Translation("Alipay"),
 *   modes = {
 *     "n/a" = @Translation("N/A"),
 *   },
 *   forms = {
 *     "offsite-payment" = "Drupal\commerce_alipayment\Form\PaymentForm",
 *     "refund-payment"  = "Drupal\commerce_alipayment\Form\PaymentRefundForm",
 *     "view-refund"  = "Drupal\commerce_alipayment\Form\ListRefundForm",
 *   },
 *   payment_type = "payment_alipay",
 *   requires_billing_information = FALSE,
 * )
 */
class Alipay extends OffsitePaymentGatewayBase implements SupportsRefundsInterface {

  //退款状态常量
  const REFUND_NEW = 'NEW';//退款新建

  const REFUND_SUCCESS = 'SUCCESS';//退款成功

  const REFUND_FAIL = 'FAIL';//退款失败

  //日志服务 默认为：\Drupal::logger('commerce_alipayment');
  protected $logger = NULL;

  //默认配置
  public function defaultConfiguration() {
    $defaultConfiguration = [
      //系统配置
      //是否收集客户账单信息
      'collect_billing_information' => FALSE,

      //app ID 默认为''
      'appId'                       => '',

      //商户应用私钥 例如：MIIEvQIBADANB ... ...
      'merchantPrivateKey'          => '',

      //商户应用公钥证书文件的内容 如:appCertPublicKey_2019051064521003.crt
      'merchantCert'                => '',

      //下载的支付宝公钥证书文件的内容 公钥证书文件如: alipayCertPublicKey_RSA2.crt
      'alipayCert'                  => '',

      //下载的支付宝根证书文件的内容 如:alipayRootCert.crt
      'alipayRootCert'              => '',

      //网关协议
      'protocol'                    => 'https',

      //网关地址
      'gatewayHost'                 => 'openapi.alipay.com',

      //加密类型  商户生成签名字符串所使用的签名算法类型，目前支持RSA2和RSA，推荐使用RSA2
      'signType'                    => 'RSA2',

      //支付超期时间，超过该时间间隔将不能发起支付，超过该期限且未支付的支付实体将被系统净化删除
      'expires'                     => 604800,//默认为7天，7*24*60*60

      //系统ID,在同一套接口账号下，如果安装了多套本系统，可能出现订单重复而无法付款，因此以此区分不同系统的订单
      'systemId'                    => 'yunke',
    ];
    $defaultConfiguration += parent::defaultConfiguration();
    return $defaultConfiguration;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['notice'] = [
      '#markup' => $this->t('If you are in trouble and need help, To contact <a href="http://www.indrupal.com" target="_blank">Yunke</a> (Wechat ID: indrupal) or <a href="http://www.will-nice.com" target="_blank">Will-Nice</a>'),
    ];

    $form['appId'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('APP ID'),
      '#required'      => TRUE,
      '#default_value' => $this->configuration['appId'],
      '#attributes'    => [
        'autocomplete' => 'off',
      ],
    ];

    $form['merchantPrivateKey'] = [
      '#type'          => 'textarea',
      '#title'         => $this->t('APP Private Key'),//应用私钥
      '#description'   => $this->t('The contents of the app private key'),
      '#default_value' => $this->configuration['merchantPrivateKey'],
      '#required'      => TRUE,
      '#attributes'    => [
        'autocomplete' => 'off',
      ],
    ];

    $form['merchantCert'] = [
      '#type'          => 'textarea',
      '#title'         => $this->t('APP Cert'),//应用公钥证书
      '#description'   => $this->t('The contents of the app cert , example: appCertPublicKey_2021002146640411.crt'),
      '#default_value' => $this->configuration['merchantCert'],
      '#required'      => TRUE,
      '#attributes'    => [
        'autocomplete' => 'off',
      ],
    ];

    $form['alipayCert'] = [
      '#type'          => 'textarea',
      '#title'         => $this->t('Alipay Cert'),//下载的支付宝公钥证书内容
      '#description'   => $this->t('The contents of alipayCert , example: alipayCertPublicKey_RSA2.crt'),
      '#default_value' => $this->configuration['alipayCert'],
      '#required'      => TRUE,
      '#attributes'    => [
        'autocomplete' => 'off',
      ],
    ];

    $form['alipayRootCert'] = [
      '#type'          => 'textarea',
      '#title'         => $this->t('Alipay Root Cert'),//下载的支付宝根证书内容
      '#description'   => $this->t('The contents of Alipay Root Cert , example: alipayRootCert.crt'),
      '#default_value' => $this->configuration['alipayRootCert'],
      '#required'      => TRUE,
      '#attributes'    => [
        'autocomplete' => 'off',
      ],
    ];

    $form['protocol'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('protocol'),//网关协议
      '#description'   => $this->t('gateway protocol, example: https'),
      '#required'      => TRUE,
      '#default_value' => $this->configuration['protocol'],
      '#attributes'    => [
        'autocomplete' => 'off',
      ],
    ];

    $form['gatewayHost'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('gateway Host'),//网关地址
      '#description'   => $this->t('gateway Host, example: openapi.alipay.com'),
      '#required'      => TRUE,
      '#default_value' => $this->configuration['gatewayHost'],
      '#attributes'    => [
        'autocomplete' => 'off',
      ],
    ];

    $form['signType'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('sign Type'),//签名算法类型
      '#description'   => $this->t('Signature algorithm type, example: RSA2 / RSA'),
      '#required'      => TRUE,
      '#default_value' => $this->configuration['signType'],
      '#attributes'    => [
        'autocomplete' => 'off',
      ],
    ];

    $form['expires'] = [
      '#type'          => 'number',
      '#title'         => $this->t('Expires time interval'),
      '#description'   => $this->t('Payments that exceed this time interval can not be paid, in second, 5 minutes ～ 15 days, default 7 days'),
      '#step'          => 1,
      '#min'           => 300,
      '#max'           => 1296000,
      '#field_suffix'  => $this->t('second'),
      '#required'      => TRUE,
      '#default_value' => $this->configuration['expires'],
      '#attributes'    => [
        'autocomplete' => 'off',
      ],
    ];

    $form['systemId'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('System ID'),//系统实例ID，在同一个接口账号下，避免多个系统中订单号重复导致的无法付款
      '#description'   => $this->t('Only 0-9 letters,To avoid duplicate order numbers for multiple systems on the same api config'),
      '#default_value' => $this->configuration['systemId'],
      '#pattern'       => '^[A-Za-z]{0,9}$',
      '#attributes'    => [
        'autocomplete' => 'off',
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);

      $this->configuration['appId'] = $values['appId'];
      $this->configuration['merchantPrivateKey'] = str_replace("\r\n", "\n", $values['merchantPrivateKey']);
      $this->configuration['merchantCert'] = str_replace("\r\n", "\n", $values['merchantCert']);
      $this->configuration['alipayCert'] = str_replace("\r\n", "\n", $values['alipayCert']);
      $this->configuration['alipayRootCert'] = str_replace("\r\n", "\n", $values['alipayRootCert']);
      $this->configuration['protocol'] = $values['protocol'];
      $this->configuration['gatewayHost'] = $values['gatewayHost'];
      $this->configuration['signType'] = $values['signType'];
      $this->configuration['expires'] = $values['expires'];
      $this->configuration['systemId'] = $values['systemId'];

      $this->configuration += $this->defaultConfiguration();
    }
  }


  /**
   * {@inheritdoc}
   */
  public function onNotify(Request $request) {
    /**
     * 处理付款的异步通知
     * 该方法将在路由commerce_payment.notify中调用，调用方法：
     * \Drupal\commerce_payment\Controller\PaymentNotificationController::notifyPage
     */
    if (!$this->logger) {
      $this->logger = \Drupal::logger('commerce_alipayment');
    }

    $alipayAPI = new AlipayAPI($this->getConfiguration(), $this->logger);

    //调用接口验证签名
    if (!$alipayAPI->verifyNotify($_POST)) {
      //验签失败
      $this->logger->warning($this->t('Alipay Pay asynchronous notification signature verification failed !'));
      return new JsonResponse(['code' => 'ERROR', "message" => "signature verification failed"], 400); //返回给支付宝的字符只要不是“success”就被认为有错
    }

    $out_trade_no = $_POST['out_trade_no']; //商家系统订单号
    $trade_no = $_POST['trade_no']; //支付宝订单号 支付宝交易号，支付宝交易凭证号
    $buyer_id = $_POST['buyer_id']; //买家支付宝用户号。买家支付宝账号对应的支付宝唯一用户号。以 2088 开头的纯 16 位数字。
    $trade_status = $_POST['trade_status']; //交易状态
    $receipt_amount = $_POST['receipt_amount']; //实收金额。商家在交易中实际收到的款项，单位为元，精确到小数点后 2 位。
    $gmt_payment = $_POST['gmt_payment']; //交易付款时间。该笔交易的买家付款时间。格式为yyyy-MM-dd HH:mm:ss。

    //只需处理付款成功的通知
    if (!in_array($trade_status, ['TRADE_SUCCESS', 'TRADE_FINISHED'])) {
      return new Response('success');//这样回复是避免支付宝进行多次通知
    }
    $paymentID = (int) OrderIDConverter::toDrupal($out_trade_no, $this->configuration['systemId']);
    $payment = \Drupal::entityTypeManager()->getStorage('commerce_payment')->load($paymentID);
    if (empty($payment)) {
      $this->logger->warning($this->t('In pay asynchronous notifications, Payment entity(id:@id) does not exist', ['@id' => $paymentID]));
      return new Response('success');
    }
    if (\Drupal::service('commerce_price.minor_units_converter')->toMinorUnits($payment->getAmount()) != (int) ($receipt_amount * 100)) {
      //支付金额不正确 将不处理
      $this->logger->warning($this->t('In pay asynchronous notifications, Payment entity(id:@id) amount or currency is abnormal', ['@id' => $paymentID]));
      return new Response('success');
    }

    //现在可以判定支付已成功 开始更新支付实体
    $payment->setCompletedTime(strtotime($gmt_payment));
    $payment->setRemoteId($trade_no);
    $payment->setRemoteState($trade_status);
    $payment->setState('completed');
    $payment->save();
    return new Response('success');
  }

  /**
   * {@inheritdoc}
   */
  public function onCancel(OrderInterface $order, Request $request) {
    parent::onCancel($order, $request);//你可在此处自定义取消支付提示消息
  }


  /**
   * {@inheritdoc}
   */
  public function canRefundPayment(PaymentInterface $payment) {
    $payment_state = $payment->getState()->getId();
    return in_array($payment_state, ['completed', 'partially_refunded']);
  }

  /**
   * {@inheritdoc}
   */
  public function buildPaymentOperations(PaymentInterface $payment) {
    $payment_state = $payment->getState()->getId();
    $operations = [];
    $operations['refund'] = [
      'title'       => $this->t('Refund'),
      'page_title'  => $this->t('Refund payment'),
      'plugin_form' => 'refund-payment',
      'access'      => $this->canRefundPayment($payment),
    ];
    $operations['view_refund'] = [
      'title'       => $this->t('View refund'),
      'page_title'  => $this->t('View refund Details'),
      'plugin_form' => 'view-refund',
      'access'      => in_array($payment_state, ['pending', 'partially_refunded', 'refunded']),
    ];
    return $operations;
  }


  /**
   * {@inheritdoc}
   */
  public function onReturn(OrderInterface $order, Request $request) {
    /**
     * 该方法在离站支付完成后返回页面中执行，具体调用是在：
     * \Drupal\commerce_payment\Controller\PaymentCheckoutController::returnPage
     * 路由为：commerce_payment.checkout.return
     * 在本支付模块中会一并返回支付实体的ID
     * 本方法向支付宝服务器进行主动查询，付款失败将后退流程，成功将继续下一步
     * 不论结果如何本方法都不会去操作支付实体，这将在异步通知中进行，因为支付宝支付一定会进行异步通知
     */
    $paymentID = $request->query->get('payment');
    if (empty($paymentID)) {
      throw new PaymentGatewayException($this->t('On payment return page, missing payment entity ID'));
    }
    $payment = \Drupal::entityTypeManager()->getStorage('commerce_payment')->load((int) $paymentID);
    if (empty($payment)) {
      throw new PaymentGatewayException($this->t('On payment return page, payment entity ID: @paymentID does not exist', ['@paymentID' => $paymentID]));
    }
    if ($payment->getOrder()->id() != $order->id()) {
      //需要验证返回的支付实体属于对应订单，避免人为返回一个其他订单的已支付实体，进而攻击系统使得本订单进入下一步流程
      throw new PaymentGatewayException($this->t('On payment return page, payment entity ID:@paymentID and order ID:@orderID do not match', ['@paymentID' => $paymentID, '@orderID' => $order->id()]));
    }

    if (!$this->logger) {
      $this->logger = \Drupal::logger('commerce_alipayment');
    }
    //进行主动查询
    $alipayAPI = new AlipayAPI($this->getConfiguration(), $this->logger);

    $result = $alipayAPI->query(OrderIDConverter::toAlipay($paymentID, $this->getConfiguration()['systemId']));
    if ($result == FALSE) {
      throw new PaymentGatewayException($this->t('On payment return page, Payment query failed'));
    }
    if (!isset($result['code']) || $result['code'] != 10000) {
      //业务层面订单查询异常  可能是订单不存在或业务繁忙
      throw new PaymentGatewayException($this->t('On payment return page, Payment query failed'));
    }

    if (isset($result['trade_status']) && $result['trade_status'] == 'TRADE_SUCCESS') {
      //进一步检查支付金额
      $paymentTotal = \Drupal::service('commerce_price.minor_units_converter')->toMinorUnits($payment->getAmount());
      $payerTotal = $result['receipt_amount'] ?: 0; // 也可设置为：$result['total_amount']
      if ($paymentTotal == (int) ($payerTotal * 100)) {
        //只有当这条件出现才被认为是支付成功 直接返回即可进入下一步流程
        return TRUE;
      }
    }

    /**
     * 此处你可能会考虑到这种情况：
     * 1、支付宝服务器出现延迟，用户已支付，但查询尚未成功，导致界面提示未付款
     * 当以上情况出现时，用户实际已经付款，但浏览器显示订单未付款，且没有进入下一步，此时会发生什么呢？
     * 实际上在付款异步通知到来后，系统会自动更新订单实体的状态，包括当前所处检出流程，不管浏览器停留在什么地方都不会对后台造成影响
     * 只需要用户刷新即可，或者直接到用户中心查看。详见订单更新服务：“commerce_payment.order_updater”
     */
    $this->messenger()->addStatus($this->t('If you have paid, please check your order at the account center later'));
    throw new PaymentGatewayException($this->t('On payment return page, Payment Failed, trade_state: @trade_state', ['@trade_state' => $result['trade_status']]));
  }


  /**
   * {@inheritdoc}
   */
  public function refundPayment(PaymentInterface $payment, Price $amount = NULL) {
    /**
     * 支付网关插件实例化时会在以下方法中补充设置本插件的操作表单释文（故查看缓存的插件定义无法看到）：
     * \Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\PaymentGatewayBase::getDefaultForms
     * 如果实现了退款接口，那么会在释文的表单项中补充以下系统默认的退款操作表单：
     * 操作名：refund-payment，表单类：Drupal\commerce_payment\PluginForm\PaymentRefundForm
     */
    $this->assertPaymentState($payment, ['completed', 'partially_refunded']);
    $amount = $amount ?: $payment->getAmount();
    $this->assertRefundAmount($payment, $amount);
    $config = $this->getConfiguration();//插件配置，即支付宝API接口配置
    $orderEntity = $payment->getOrder(); //订单实体
    if (!$this->logger) {
      $this->logger = \Drupal::logger('commerce_alipayment');
    }
    $alipayAPI = new AlipayAPI($config, $this->logger);
    $paymentID = $payment->id();

    /**
     * 构建退款订单数组：
     */
    $time = (string) time();//用作退款ID标识
    $order['out_trade_no'] = OrderIDConverter::toAlipay($paymentID, $config['systemId']);
    //原支付订单号
    $order['refund_amount'] = \Drupal::service('commerce_price.minor_units_converter')->toMinorUnits($amount);
    //退款金额
    $order['out_request_no'] = $order['out_trade_no'] . '_' . $time;
    //退款单号  String  最大长度64
    $refund_reason = $this->t('Refund') . ':';
    if (isset($orderEntity->getItems()[0])) {
      $refund_reason .= Unicode::truncate($orderEntity->getItems()[0]->getTitle(), 20, FALSE, TRUE);
    }
    $order['refund_reason'] = $refund_reason . $this->t('OrderID') . ':' . $orderEntity->id();
    $order['currency'] = $payment->getAmount()->getCurrencyCode();
    //货币代码

    //退款信息初始化数据
    $refundData = [
      'state'         => static::REFUND_NEW,
      'refund_amount' => $order['refund_amount'], //注意这里是以分为单位保存
      'currency'      => $order['currency'],
      'data'          => [], //用于保存支付宝返回的数据，以便查询
    ];
    $moduleData = $orderEntity->getData(COMMERCE_ALIPAYMENT_DATA_KEY, []);
    if (!isset($moduleData['refund'][$paymentID])) {
      $moduleData['refund'][$paymentID] = [];
    }
    $moduleData['refund'][$paymentID][$time] = $refundData;
    $orderEntity->setData(COMMERCE_ALIPAYMENT_DATA_KEY, $moduleData);
    $orderEntity->save(); //先保存一次  以防退款失败

    $result = $alipayAPI->refund($order);
    if ($result === FALSE) {
      $this->logger->warning('Alipay Pay failed to refund');
      throw new PaymentGatewayException($this->t('Alipay Pay failed to refund'));
    }
    $data = print_r($result, TRUE);
    if (!isset($result['code']) || $result['code'] != 10000) {
      //业务层面订单查询异常  可能是订单不存在
      $message = 'Alipay Pay failed to refund, return: ' . $data;
      $this->logger->warning($message);
      $refundData = [
        'state'         => static::REFUND_FAIL,
        'refund_amount' => $order['refund_amount'],
        'currency'      => $order['currency'],
        'data'          => $data,
      ];
      $moduleData['refund'][$paymentID][$time] = $refundData;
      $orderEntity->setData(COMMERCE_ALIPAYMENT_DATA_KEY, $moduleData);
      $orderEntity->save(); //保存一次  指示退款失败
      throw new PaymentGatewayException($this->t('Alipay Pay failed to refund'));
    }
    if (isset($result['fund_change']) && $result['fund_change'] == 'Y') {      //退款成功
      $refundData['state'] = static::REFUND_SUCCESS;
      $refundData['data'] = $data;
      $old_refunded_amount = $payment->getRefundedAmount();
      if (!empty($old_refunded_amount)) {
        $new_refunded_amount = $amount->add($old_refunded_amount);
      }
      else {
        $new_refunded_amount = $amount;
      }
      if ($new_refunded_amount->lessThan($payment->getAmount())) {
        $payment->state = 'partially_refunded';
      }
      else {
        $payment->state = 'refunded';
      }
      $payment->setRefundedAmount($new_refunded_amount);
      $moduleData['refund'][$paymentID][$time] = $refundData;
      $orderEntity->setData(COMMERCE_ALIPAYMENT_DATA_KEY, $moduleData);
      $orderEntity->save();
      $payment->save();
      return true;
    }
    else {
      $refundData['state'] = static::REFUND_FAIL;
      $refundData['data'] = $data;
      $moduleData['refund'][$paymentID][$time] = $refundData;
      $orderEntity->setData(COMMERCE_ALIPAYMENT_DATA_KEY, $moduleData);
      $orderEntity->save();
      return false;
    }
  }


}
