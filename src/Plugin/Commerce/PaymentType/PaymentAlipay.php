<?php

namespace Drupal\commerce_alipayment\Plugin\Commerce\PaymentType;

use Drupal\commerce_payment\Plugin\Commerce\PaymentType\PaymentTypeBase;
/**
 * Provides the alipay payment type.
 *
 * @CommercePaymentType(
 *   id = "payment_alipay",
 *   label = @Translation("Alipay"),
 *   workflow = "payment_alipay",
 * )
 */
class PaymentAlipay extends PaymentTypeBase {

  /**
   * {@inheritdoc}
   */
  public function buildFieldDefinitions() {
    return [];
  }

}
